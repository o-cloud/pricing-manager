package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/irt-sb1/pricing-manager/config"
	"gitlab.com/irt-sb1/pricing-manager/pricing_handler"
)

func main() {
	config.Load()
	r := setupRouter()

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	pricing_handler.NewPricingHandler().SetupRoutes(r.Group("/api/pricing-manager"))
	return r
}
