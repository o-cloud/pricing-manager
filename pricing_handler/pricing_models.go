package pricing_handler

import "go.mongodb.org/mongo-driver/bson/primitive"

type ResourceRates struct {
	Cpu                float64 `json:"cpu" bson:"cpu"`
	Memory             float64 `json:"memory" bson:"memory"`
	NetworkTransmitted float64 `json:"networkTransmitted" bson:"networkTransmitted"`
	NetworkReceived    float64 `json:"networkReceived" bson:"networkReceived"`
}

type BasicRates struct {
	Name  string  `json:"name" bson:"name"`
	Value float64 `json:"value" bson:"value"`
	Type  string  `json:"type" bson:"type"`
}

type BasicRatesFront struct {
	Name    string             `json:"name" bson:"name"`
	Value   float64            `json:"value" bson:"value"`
	Type    string             `json:"type" bson:"type"`
	IdRates primitive.ObjectID `json:"_id" bson:"_id"`
}

type BasicRemoteRatesFront struct {
	OriginClusterName string             `json:"originClusterName" bson:"originClusterName"`
	PeerURL           string             `json:"peerURL" bson:"peerURL"`
	Rates             BasicRates         `json:"rates" bson:"rates"`
	IdRates           primitive.ObjectID `json:"_id" bson:"_id"`
}

type BasicRemoteRates struct {
	OriginClusterName string     `json:"originClusterName" bson:"originClusterName"`
	PeerURL           string     `json:"peerURL" bson:"peerURL"`
	Rates             BasicRates `json:"rates" bson:"rates"`
}
