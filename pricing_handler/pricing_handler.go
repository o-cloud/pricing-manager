package pricing_handler

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (P PricingHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/rates/resources/:collection", P.getRates)
	router.GET("/rates/provider/:originClusterName/:providerId", P.getRemoteProviderRatesFromID)
	router.GET("/rates/computing/:originClusterName", P.getRemoteComputingRates)
	router.GET("/rates/computing", P.getComputingRates)
	router.GET("/rates/provider/providerId/:providerId", P.getLocalRatesFromProviderID)
	router.GET("/rates/resources/:collection/:id", P.getRatesById)
	router.POST("/rates/resources/:collection", P.setRates)
	router.PUT("/rates/resources/:collection/:id", P.updateRates)
	router.DELETE("/rates/resources/:collection/:id", P.deleteRates)
}

type PricingHandler struct {
	Service *mongo.Database
}

func NewPricingHandler() PricingHandler {
	pricingHandler := PricingHandler{
		Service: GetPricingHandler(),
	}
	err := pricingHandler.loadDefaultRatesIntoDB()
	if err != nil {
		fmt.Println("Error while loading default rates into DB: ", err)
	}
	return pricingHandler
}

func (handler *PricingHandler) getLocalRatesFromProviderID(context *gin.Context) {
	// Retrieve rates of local provider
	providerID := context.Param("providerId")
	rates, err := handler.getProviderRatesFromCollection("MyResources", providerID)
	if err != nil {
		log.Println("Error while retrieving data from collection: ", err)
		context.JSON(http.StatusInternalServerError, rates)
	}
	context.JSON(http.StatusOK, rates)
}

func (handler *PricingHandler) getRemoteProviderRatesFromID(context *gin.Context) {
	// Retrieve rates of remote provider

	// Object to store results rates
	remoteRates := BasicRemoteRates{}
	providerID := context.Param("providerId")
	remoteRates.OriginClusterName = context.Param("originClusterName")

	// Check if rates already in DB
	isInDB, err := handler.isRateInDB("RemoteResources", bson.M{"rates.name": providerID, "originClusterName": remoteRates.OriginClusterName})
	if err != nil {
		_ = context.Error(err)
		context.Status(http.StatusInternalServerError)
		return
	}

	if isInDB {
		context.JSON(http.StatusOK, "Rates already in DB")
		return
	}

	// Get list of peers in network
	peerResponse := handler.GetPeersInNetwork()

	// Get peer of interest
	remoteRates.PeerURL = handler.GetURLFromPeerResponse(remoteRates.OriginClusterName, peerResponse)

	// Retrieve rates from peer of interested for requested provider
	remoteRates.Rates, err = handler.GetRemoteProviderRatesFromId(remoteRates.PeerURL, providerID)
	if err != nil {
		fmt.Println("Error retrieving remote rates: ", err)
		context.JSON(http.StatusNotFound, err)
	}

	// If response is empty we do not store in DB
	if (remoteRates.Rates == BasicRates{}) {
		context.JSON(http.StatusOK, "No rates were retrieved")
		return
	}

	err = handler.insertRateInDB("RemoteResources", remoteRates)
	if err != nil {
		fmt.Println("Error while storing remote rates in DB: ", err)
	}

	context.JSON(http.StatusOK, "")
}

func (handler *PricingHandler) getComputingRates(context *gin.Context) {
	// Retrieve and return rates of local compute-provider
	log.Println("Retrieve and send compute-provider resources rates")

	collection := "MyResources"
	// List of compute-provider resources to retreive
	resources := []string{"CPU", "Memory", "NetworkReceived", "NetworkTransmitted"}

	//Initialize slice to store resources rates
	resourcesRate := make([]BasicRates, len(resources))

	// Retrieve rates of each resource
	for index, resource := range resources {
		res, err := handler.getProviderRatesFromCollection(collection, resource)
		if err != nil {
			log.Panic("Error while retrieving data from collection: ", err)
		}

		// Add rates of resource to returned slice
		resourcesRate[index] = res
	}
	log.Println("resourcesRates: ", resourcesRate)
	context.JSON(http.StatusOK, resourcesRate)
}

func (handler *PricingHandler) getRemoteComputingRates(context *gin.Context) {
	// Retrieve rates of remote compute provider

	originClusterName := context.Param("originClusterName")

	// Get list of peers in network
	peerResponse := handler.GetPeersInNetwork()

	// Get peer of interest
	peerURL := handler.GetURLFromPeerResponse(originClusterName, peerResponse)

	// Check if rates already in DB
	isInDB, err := handler.isRateInDB("RemoteResources", bson.M{"originClusterName": originClusterName, "peerURL": peerURL, "rates.type": "computing"})
	if err != nil {
		_ = context.Error(err)
		context.Status(http.StatusInternalServerError)
		return
	}

	if isInDB {
		context.JSON(http.StatusOK, "Rates already in DB")
		return
	}

	// Initialize slice that stores results
	remoteRates := make([]BasicRemoteRates, 4)
	for resource := range remoteRates {
		remoteRates[resource].OriginClusterName = context.Param("originClusterName")
		remoteRates[resource].PeerURL = peerURL
	}

	// Retrive remote compute-provider rates
	err = handler.GetRemoteComputingRates(&remoteRates)
	if err != nil {
		fmt.Println("Error while retrieving remote compute-provider rates: ", err)
	}
	log.Println("slice after function call: ", remoteRates)

	for _, rates := range remoteRates {
		err = handler.insertRateInDB("RemoteResources", rates)
		if err != nil {
			fmt.Println("Error while storing remote rates in DB: ", err)
		}

	}

	context.JSON(http.StatusOK, "")
}

func (handler *PricingHandler) getRates(context *gin.Context) {
	// Retrieve stored rates from DB

	fmt.Println("Rates requested")
	collection := context.Param("collection")
	res, err := handler.getRatesFromCollection(collection)
	if err != nil {
		log.Panic("Error while retrieving data from collection: ", err)
	}

	context.JSON(http.StatusOK, res)
}

func (handler *PricingHandler) getRatesById(context *gin.Context) {
	// Retrieve stored rates from DB

	collection := context.Param("collection")
	id, err := primitive.ObjectIDFromHex(context.Param("id"))
	if err != nil {
		fmt.Errorf("ERROR CREATING OBJECTID FROM PARAMETER: %v", err)
	}

	res, err := handler.getRatesFromCollectionById(collection, id)
	if err != nil {
		log.Panic("Error while retrieving data from collection: ", err)
	}

	context.JSON(http.StatusOK, res)
}

func (handler *PricingHandler) setRates(context *gin.Context) {
	var rates BasicRates
	if err := context.BindJSON(&rates); err != nil {
		log.Panic("ERROR BINDING json to rates", err)
	}

	// Check if rates is already in the database
	isInDB, err := handler.isRateInDB("MyResources", bson.M{"name": rates.Name})
	if err != nil {
		_ = context.Error(err)
		context.Status(http.StatusInternalServerError)
		return
	}

	if isInDB {
		context.JSON(http.StatusOK, "Rates already in DB")
		return
	}

	// If not present we insert rates in DB
	if err := handler.insertRateInDB("MyResources", rates); err != nil {
		_ = context.Error(err)
		context.Status(http.StatusInternalServerError)
		return
	}
	res, err := handler.getRatesFromCollection("MyResources")

	if err != nil {
		log.Panic("Error while retrieving data from collection: ", err)
	}
	context.JSON(http.StatusOK, res)
}

func (handler *PricingHandler) updateRates(context *gin.Context) {

	//Retrieve id and collection to update from URL
	id, err := primitive.ObjectIDFromHex(context.Param("id"))
	if err != nil {
		fmt.Errorf("ERROR CREATING OBJECTID FROM PARAMETER: %v", err)
	}

	collection := context.Param("collection")

	// Create Struct to store new values and bind it to request
	var rates BasicRates
	if err != nil {
		fmt.Print("Error retrieving struct for collection: ", err)
		context.Status(http.StatusBadRequest)
		return
	}

	if err := context.BindJSON(&rates); err != nil {
		log.Panic("ERROR BINDING json to rates", err)
	}

	// Update collection
	handler.updateCollectionByID(collection, rates, id)

}

func (handler *PricingHandler) deleteRates(context *gin.Context) {
	//Retrieve id and collection to update from URL
	collection := context.Param("collection")
	id, err := primitive.ObjectIDFromHex(context.Param("id"))
	if err != nil {
		fmt.Errorf("ERROR CREATING OBJECTID FROM PARAMETER: %v", err)
	}

	// Delete entry from collection
	handler.deleteCollectionEntryByID(collection, id)
}
