package pricing_handler

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/o-cloud/cluster-discovery-api/api"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func GetPricingHandler() *mongo.Database {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")))

	if err != nil {
		fmt.Printf("Error inigitalizing mongoDB client %v", err)
	}

	return client.Database("pricing")
}

func (handler *PricingHandler) loadDefaultRatesIntoDB() error {
	// Retrieve default rate file
	file, err := ioutil.ReadFile("./pricing_handler/default-rates.json")
	if err != nil {

		// Trying from another location
		file, err = ioutil.ReadFile("/etc/pricing_handler/default-rates.json")
		if err != nil {
			log.Printf("could not read config file: %v", err)
			return err
		}
	}

	defaultRates := []BasicRates{}

	// Extract rates values into dedicated struct
	err = json.Unmarshal([]byte(file), &defaultRates)
	if err != nil {
		log.Fatalf("could not decode config into struct: %v", err)
	}

	// Loop over element in slice and store them into DB
	for _, rates := range defaultRates {
		isInDB, err := handler.isRateInDB("MyResources", bson.M{"name": rates.Name})
		if err != nil {
			return err
		}

		if isInDB {
			continue
		}
		fmt.Println(rates)
		handler.insertRateInDB("MyResources", rates)
	}
	return nil
}

func (handler *PricingHandler) insertRateInDB(collection string, rates interface{}) error {
	// Insert new rates in DB
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, errInsert := handler.Service.Collection(collection).InsertOne(ctx, rates)
	return errInsert
}

func (handler *PricingHandler) getRatesFromCollection(collectionName string) (interface{}, error) {
	// Retrieve rates for a given collection
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	var results interface{}
	if collectionName == "MyResources" {
		results = []BasicRatesFront{}
	} else if collectionName == "RemoteResources" {
		results = []BasicRemoteRatesFront{}
	}

	collection, err := handler.Service.Collection(collectionName).Find(ctx, bson.M{})

	if err != nil {
		return nil, err
	}

	defer collection.Close(ctx)
	collection.All(ctx, &results)

	if err := collection.Err(); err != nil {
		return nil, err
	}

	return results, err
}

func (handler *PricingHandler) getProviderRatesFromCollection(collectionName string, providerId string) (BasicRates, error) {
	// Retrieve rates for a given collection and a given resource
	log.Println("Asked for rates of provider: ", providerId)
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	var results BasicRates

	collection := handler.Service.Collection(collectionName).FindOne(ctx, bson.M{"$or": []bson.M{{"name": providerId}, {"providerId": providerId}}})
	collection.Decode(&results)

	err := collection.Err()
	if err != nil {
		fmt.Println("Error while unmarshaling collection in struct", err)
		return BasicRates{}, err
	}
	log.Println("results from pricin.go: ", results)
	return results, err
}

func (handler *PricingHandler) isRateInDB(collectionName string, filter bson.M) (bool, error) {
	// Retrieve rates for a given collection and a given id
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	isInDB := false
	defer cancel()

	var results interface{}

	if collectionName == "MyResources" {
		results = BasicRates{}
	} else if collectionName == "RemoteResources" {
		results = BasicRemoteRates{}
	}

	// Check if rates according to filter exist
	collection := handler.Service.Collection(collectionName).FindOne(ctx, filter)

	err := collection.Decode(&results)

	// We use the mongo.ErrNoDocuments error to check if document is empty
	if err != nil && err != mongo.ErrNoDocuments { // Error while retrieving document
		log.Print("Error while retrieving DB entry by ID: ", err)
	} else if err == mongo.ErrNoDocuments { // Error is due to fact that docuement is empty
		// We reset the error
		err = nil
		return isInDB, err
	}
	isInDB = true
	return isInDB, err
}

func (handler *PricingHandler) getRatesFromCollectionById(collectionName string, id primitive.ObjectID) (interface{}, error) {
	// Retrieve rates for a given collection and a given id
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	var results interface{}
	if collectionName == "MyResources" {
		results = []BasicRates{}
	} else if collectionName == "RemoteResources" {
		results = []BasicRemoteRates{}
	}

	collection := handler.Service.Collection(collectionName).FindOne(ctx, bson.M{"_id": id})

	log.Println("collection: ", collection)

	err := collection.Decode(results)
	log.Println(results)

	if err != nil {
		log.Print("Error while retrieving DB entry by ID: ", err)
	}

	return results, err
}

func (handler *PricingHandler) isCollectionExists(collection string) bool {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	collectionList, err := handler.Service.ListCollectionNames(ctx, bson.M{})

	if err != nil {
		err = fmt.Errorf("ERROR RETRIEVING COLLECTIONS FROM DB: %v", err)
		fmt.Println(err.Error())
	}

	// Simply search in the names slice, e.g.
	for _, collectionName := range collectionList {
		if collectionName == collection {
			return true
		}
	}
	return false

}

func (handler *PricingHandler) updateCollectionByID(collectionName string, rates interface{}, id primitive.ObjectID) {
	if !handler.isCollectionExists(collectionName) {
		fmt.Println(fmt.Errorf("THE COLLECTION \"%v\" DOES NOT EXIST", collectionName))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	collection := handler.Service.Collection(collectionName)
	log.Println("Updating values in DB with rates", rates)

	_, err := collection.ReplaceOne(ctx, bson.M{"_id": id}, rates)
	log.Println(rates)

	if err != nil {
		log.Printf("ERROR UPDATING DB : %v", err)
	}
}

func (handler *PricingHandler) deleteCollectionEntryByID(collectionName string, id primitive.ObjectID) {
	if !handler.isCollectionExists(collectionName) {
		fmt.Println(fmt.Errorf("THE COLLECTION \"%v\" DOES NOT EXIST", collectionName))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	collection := handler.Service.Collection(collectionName)

	log.Println("Deleting entry with ID: ", id)
	res, err := collection.DeleteOne(ctx, bson.M{"_id": id})

	if res.DeletedCount == 0 {
		fmt.Println("DeleteOne() document not found:", res)
	}

	if err != nil {
		log.Fatal("DeleteOne() ERROR:", err)
	}
}

func (handler *PricingHandler) IsLocalIp(context *gin.Context) bool {
	if ip := context.ClientIP(); ip == "::1" {
		log.Println("IP is local")
		return true
	}
	log.Println("IP is non local")
	return false

}

func (handler *PricingHandler) GetPeersInNetwork() api.PeersResponse {
	// Ask api discovery for peers in network

	path_discovery := fmt.Sprintf("%s/api/discovery/peers", viper.GetString("CLUSTER_DISCOVERY_API"))
	result, err := http.Get(path_discovery)
	if err != nil {
		fmt.Println("Error calling cluster discovery api ", err)
	}

	jsonData, err := ioutil.ReadAll(result.Body)
	if err != nil {
		log.Println("Error reading response body into json: ", err)
	}

	// Unmarshal response into corresponding struct
	var peerResponse api.PeersResponse
	err = json.Unmarshal(jsonData, &peerResponse)
	if err != nil {
		log.Println("Error unmarshaling peerResponse: ", err)
	}

	return peerResponse
}

func (handler *PricingHandler) GetRemoteProviderRatesFromId(peerURL string, providerID string) (BasicRates, error) {
	// Call remote pricing-manager to retrieve rates of a given provider
	path_remote_pm := fmt.Sprintf("%s/api/pricing-manager/rates/provider/providerId/%s", peerURL, providerID)

	log.Println("path_remote_pm: ", path_remote_pm)

	resp, err := http.Get(path_remote_pm)
	if err != nil {
		log.Println("Error while contacting remote pricing-manager: ", err)
		return BasicRates{}, err
	}

	results := BasicRates{}
	// var results BasicRates
	jsonData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading response body into json: ", err)
	}
	err = json.Unmarshal(jsonData, &results)
	if err != nil {
		log.Println("Error unmarshaling peerResponse: ", err)
	}
	log.Println(results)

	return results, nil

}

func (handler *PricingHandler) GetRemoteComputingRates(remoteRates *[]BasicRemoteRates) error {
	// Call remote pricing-manager to retrieve rates from compute-provider resources
	path_remote_pm := fmt.Sprintf("%s/api/pricing-manager/rates/computing", (*remoteRates)[0].PeerURL)

	log.Println("path_remote_pm: ", path_remote_pm)

	// Call remote pricing-manager to retrieve rates of compute-provider
	resp, err := http.Get(path_remote_pm)
	if err != nil {
		log.Println("Error while contacting remote pricing-manager: ", err)
	}

	results := []BasicRates{}

	jsonData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading response body into json: ", err)
	}
	err = json.Unmarshal(jsonData, &results)
	if err != nil {
		log.Println("Error unmarshaling response: ", err)
	}

	// Add the retrieved rates in the dedicated structure
	for index := range *remoteRates {
		(*remoteRates)[index].Rates = results[index]
	}

	return err

}

func (handler *PricingHandler) GetURLFromPeerResponse(peerName string, peerResponse api.PeersResponse) string {
	var peerURL string
	// Loop over peers and check their names
	for _, peer := range peerResponse.Peers {
		if peer.Name == peerName {
			peerURL = peer.URL
		}
	}
	return peerURL
}
