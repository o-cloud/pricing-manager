# Builder Image
FROM golang:1.16 AS builder

WORKDIR /usr/src

COPY ./go.mod ./go.sum ./
# COPY ./go.mod ./go.sum ./pricing_handler/default-rates.json ./
# COPY ./pricing_handler/default-rates.json ./

#COPY ./mongodemo/mongodemo_client/go.mod ./mongodemo/mongodemo_client/go.sum ./mongodemo/mongodemo_client/

RUN go mod download 

COPY . .

RUN CGO_ENABLED=0 go build -o /go/bin/go-microservice-server

# Final image
FROM scratch
# FROM busybox
COPY ./config.yaml /etc/irtsb/
COPY ./pricing_handler/default-rates.json /etc/pricing_handler/
COPY --from=builder /go/bin/go-microservice-server /go/bin/go-microservice-server

ENTRYPOINT ["/go/bin/go-microservice-server"]

