package config

import (
	"log"

	"github.com/spf13/viper"
)

var (
	Config ServiceConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)

	// Find and read the config file
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	//Environment variables
	viper.SetDefault("MONGO_CON_STRING", "mongodb://localhost:27017/")
	viper.BindEnv("MONGO_CON_STRING")
	viper.SetDefault("CLUSTER_DISCOVERY_API", "http://cluster-discovery-api:9090")
	viper.BindEnv("CLUSTER_DISCOVERY_API")
}

type ServiceConfig struct {
	MongoDbUri string
	Server     ServerServiceConfig
}

type ServerServiceConfig struct {
	ListenAddress string
}
