module gitlab.com/irt-sb1/pricing-manager

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20200217142428-fce0ec30dd00 // indirect
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/smartystreets/assertions v0.0.0-20190401211740-f487f9de1cd3 // indirect
	github.com/spf13/viper v1.8.1
	github.com/tidwall/pretty v1.1.0 // indirect
	gitlab.com/o-cloud/cluster-discovery-api v0.0.0-20211021122147-299fd520c0b0
	go.mongodb.org/mongo-driver v1.5.2
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
