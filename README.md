# Pricing manager
## Overview
The pricing-manager is in charge of:
* Storing, retrieving and modifying the rates of the ressources we provide
* Asking and storing the rates of remote providers

The pricing-manager used mongoDB to store the rates.

### List of endpoints
| Method | Endpoint     | Function  |   Action   |
| ------ |--------------|-----------|------------|
| GET |` /rates/resources/:collection`                   | `PricingHandler.getRates`            | Get all rates for a given collection          |
| GET |` /rates/provider/:originClusterName/:providerId` | `PricingHandler.getRemoteProviderRatesFromID` |  Retrieve rates of provider from remote cluster |
| GET |` /rates/computing/:originClusterName`            | `PricngHandler.getRemoteComputingRates` | Ask local pricing-manager to retrieve remote compute-provider rates | 
| GET |` /rates/computing`                               | `PricingHandler.getComputingRates` |  Retrieve remote compute-provider rates                     |
| GET |` /rates/provider/providerId/:providerId`         | `PricingHandler.getLocalRatesFromProviderID` | Retrieve rates of local provider
| GET |` /rates/resources/:collection/:id`               | `PricingHandler.getRatesById`        | Get rates for a given collection by ID  |
| POST |` /rates/resources/:collection`                   | `PricingHandler.setRates`            | Set rates to DB         |
| PUT |` /rates/resources/:collection/:id`               | `PricingHandler.updateRates`         | Update existing rates in DB      |
| DELETE |` /rates/resources/:collection/:id`               | `PricingHandler.deleteRates`         | Delete rates in DB      |


# Basic diagram of pricing-manager layout
```plantuml
@startuml
participant frontend as fe
database mongo1 as DB1
participant genericbackend1 as gb1
participant pricingmanager1 as pm1
participant pricingmanager2 as pm2
participant catalog2 as catalog2
database mongo2 as DB2
fe -> gb1  : Add favorite
gb1 -> gb1 : Check service type (computing or other)
gb1 -> pm1 : Send clusterName and resource to pricing-manager to retrieve remote resource cost
pm1 -> pm2 : Request resource rates from remote providers for cost calculation
pm2 -> DB2 : Retrieve resource rates from DB
pm2 -> pm1 : Return rates to pm1 
pm1 -> DB1 : Store retrieved rates in DB if not present
gb1 <-> DB1 : Retrieve rates from DB for cost calculation
@enduml

```

## Pricing-manager and resources type
All source types are treated the same way by the pricing-manager except the `computing` type. 
This is due to the fact that the `computing` resources contains in fact several resources, i.e., CPU, memory, network transmitted and network received.
Other services such as `data` and `service` resource contain only one resource.
For this reason, a check is made, by the `generic-backend`, on the type of the source that is added to favorites, and a different endpoint is used by the pricing-manager to retrieve `computing` rates
from the partner.

## Pricing-manager and mongoDB
The `pricing-manager` uses mongoDB to store the rates. 
Two collections are created by the `pricing-manager`:
* `MyResources` that stores the rates of the resources the pair is providing
* `RemoteResources` that stores the rates of the resources from remote providers. The remote resource has to be added to favorites for its rates to be stored in this collection.

### Structure of rates stored in DB
#### Collection: `MyResources`
```json
[
{"name":"SentinelProviderDefault","value":147,"type":"data"}
  {  
     "_id": "624564543457cd42fdbfac5e",
     "name": "CPU",
     "value": 123,
     "type": "computing"
  },
  {
     "_id": "624564543457cd42fdbfac5f",
      "name": "Memory",
      "value": 123,
      "type": "computing"
  },
  {
     "_id": "624564543457cd42fdbfac60",
      "name": "SentinelProviderDefault",
      "value": 147,
      "type": "data"
  },
  {
     "_id": "624564543457cd42fdbfac61",
      "name": "image_converter",
      "value": 1.23,
      "type": "service"
  }
       
]

```


#### Collection: `RemoteResources`
```json
{
    "_id":"6231b686f72e3cbd33f1e992",
    "originClusterName":"sbotest2",
    "peerURL":"http://34.76.190.242",
    "rates":
        {
            "name":"CPU",
            "value":123,
            "type":"computing"
        }
}
```
